package zadanie;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Autor extends BaseEntity{
    public Autor(String imie, String nazwisko, int rokUrodzenia, String miejsceUrodzenia) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
        this.miejsceUrodzenia = miejsceUrodzenia;

    }
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
private Long id;
    private String imie;
    private String nazwisko;
    private int rokUrodzenia;
    private String miejsceUrodzenia;
    @OneToMany(mappedBy = "autor", fetch = FetchType.EAGER)
    @ToString.Exclude
    private List<Ksiazka> ksiazka = new ArrayList<>();
}
