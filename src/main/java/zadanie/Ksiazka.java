package zadanie;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor


public class Ksiazka extends  BaseEntity {
    public Ksiazka(String tytul, int rokWydania, TypKsiazki typKsiazki, int iloscStron) {
        this.tytul = tytul;
        this.rokWydania = rokWydania;
        this.typKsiazki = typKsiazki;
        this.iloscStron = iloscStron;

    }
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String tytul;
    private int rokWydania;
    @Enumerated (EnumType.STRING)
    private TypKsiazki typKsiazki;
    private int iloscStron;
   ;
@ManyToOne
    @ToString.Exclude
    private Autor autor;
}
