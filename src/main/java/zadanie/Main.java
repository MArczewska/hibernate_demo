package zadanie;


import java.util.List;

public class Main {
    public static void main(String[] args) {

        Ksiazka ksiazka = new Ksiazka("Bajki", 2014, TypKsiazki.AKCJI, 20);
        Ksiazka ksiazka1 = new Ksiazka("Rowery dwa", 2010, TypKsiazki.NAUKOWA, 50);
        Ksiazka ksiazka2 = new Ksiazka("Bolek i Lolek", 2000, TypKsiazki.AKCJI, 20);


        Autor autor = new Autor("Krzysztof", "Kowalski", 1951, "Warszawa");
        Dao.saveEntity(autor);
        Dao.saveEntity(ksiazka);
        Dao.saveEntity(ksiazka1);
        Dao.saveEntity(ksiazka2);


        autor.getKsiazka().add(ksiazka);
        autor.getKsiazka().add(ksiazka1);
        autor.getKsiazka().add(ksiazka2);
        ksiazka.setAutor(autor);
        ksiazka1.setAutor(autor);
        ksiazka2.setAutor(autor);


        Dao.saveEntity(autor);
        Dao.saveEntity(ksiazka);
        Dao.saveEntity(ksiazka1);
        Dao.saveEntity(ksiazka2);

        List<Ksiazka> ksiazkaList = Dao.getAll(Ksiazka.class);
        System.out.println(ksiazkaList);
        List<Autor> autorList = Dao.getAll(Autor.class);
        System.out.println(autorList);

    }
}
